/* Author: Reilly Kearney */
/* Homework 1, Problem 1 */

#include <stdio.h>

int main(  ) {
	
	printf( "hex: %1$hhx \tunsigned: %1$hhu \tsigned: %1$hhd\n",  -85 );
	printf( "hex: %1$hhx \tunsigned: %1$hhu \tsigned: %1$hhd\n",   18  );
	printf( "hex: %1$hhx \tunsigned: %1$hhu \tsigned: %1$hhd\n", 0xcd );

	return 0;
}
