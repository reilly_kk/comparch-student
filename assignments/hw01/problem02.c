/* Author: Reilly Kearney */
/* Homework 1, Problem 2 */

#include <stdio.h>
#include <stdlib.h>

// function prototype
void printbin( char n );

int main(  ) {
	printbin( -128 );
	printbin( -65 );
	printbin( -5 );
	printbin( -1 );
	printbin( 0 );
	printbin( 1 );
	printbin( 5 );
	printbin( 65 );
	printbin( 127 );
    
	return 0;
}

// functions
void printbin( char n ) {
	int binary[ 8 ] = { 0 };
	int r, num, j, i = 0, check = 0;

	num = n;
	printf( "%d as binary is ", num );

	if ( num < 0 ) {
		check = 1;	// Store if number was negative for later
	}
	num = abs( num );	// make number positive for binary conversion

	/* Convert to Binary Number */
	while ( num != 0 ) {
		r = num%2;			// get remainder
        num = num/2;		// divide num by 2
        binary[ i++ ] = r;	// store remainder (bit)
    }

    if ( check ) {
		for ( i = 0; i < 8; i++ )
	    	binary[ i ] = !binary[ i ];
	    for ( i = 0; i < 8; i++ ) {
            if ( binary[ i ] == 1 ) {
	    		binary [ i ] = 0;
            }
            else {
	    		binary [ i ] = 1;
	    		break;
	    	}
	   	}
	}

    /* Display Binary Number */
	for ( j = 7; j >= 0; j-- )
		printf( "%d", binary[ j ] );
    printf("\n");
}