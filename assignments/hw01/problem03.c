/* Author: Reilly Kearney */
/* Homework 1, Problem 3 */

#include <stdio.h>

// function prototype
unsigned bitrange( unsigned inst, unsigned hi, unsigned lo );

int main( ) {
	
	unsigned inst = 0xabcd1234;

	printf( "inst = %x\n", inst );
	printf( "bitrange(inst,  3,  0) == %x\n", bitrange( inst,  3,  0 ) ); // 0x4
	printf( "bitrange(inst, 31, 28) == %x\n", bitrange( inst, 31, 28 ) ); // 0xa
    printf( "bitrange(inst, 27,  4) == %x\n", bitrange( inst, 27,  4 ) ); // 0xbcd123
	printf( "bitrange(inst,  7,  4) == %x\n", bitrange( inst,  7,  4 ) ); // 0x3
	printf( "bitrange(inst, 15,  8) == %x\n", bitrange( inst, 15,  8 ) ); // 0x12
	printf( "bitrange(inst, 31,  0) == %x\n", bitrange( inst, 31,  0 ) ); // 0x12

	return 0;
}

unsigned bitrange( unsigned inst, unsigned hi, unsigned lo ) {

	if ( ( hi - lo + 1 ) == 32 ) {
		return inst;
	} 

	unsigned mask = ( 1 << ( hi - lo + 1 ) ) - 1;
	return ( inst >> lo ) & mask;

}
