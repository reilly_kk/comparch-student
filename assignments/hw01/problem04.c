/* Author: Reilly Kearney */
/* Homework 1, Problem 4 */

#include <stdio.h>

// function prototype
int signext( int n, unsigned s );

int main(  ) {
	
	printf( "signext(   0x8f,  8 ) == %08x\n", signext(   0x8f,  8 ) ); // 0xffffff8f
    printf( "signext(  0x18f, 12 ) == %08x\n", signext(  0x18f, 12 ) ); // 0x0000018f
    printf( "signext(   0x8f,  9 ) == %08x\n", signext(   0x8f,  9 ) ); // 0x0000008f
    printf( "signext( 0xabcd, 16 ) == %08x\n", signext( 0xabcd, 16 ) ); // 0xffffabcd

	return 0;
}

// functions
int signext( int n, unsigned s ) {

	n = n << ( 32 - s );
	return ( n >> ( 32 - s ) );

}