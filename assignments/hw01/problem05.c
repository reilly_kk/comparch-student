/* Author: Reilly Kearney */
/* Homework 1, Problem 5 */

#include <stdio.h>

// function prototype
int concat( short m, short n );

int main(  ) {
	
	printf( "\tNew Integer: %08x\n", concat( 0x1001, 0x0011 ) );
	printf( "\tNew Integer: %08x\n", concat( 0x1010, 0x0101 ) );
	printf( "\tNew Integer: %08x\n", concat( 0x1011, 0x1111 ) );
	printf( "\tNew Integer: %08x\n", concat( 0x1101, 0x0011 ) );

	return 0;
}

// functions
int concat( short m, short n ) {

	printf( "m: %04x n: %04x", m, n );
	return ( ( m & 0xffff ) << 16) | ( n & 0xffff );

}