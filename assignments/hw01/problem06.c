/* Author: Reilly Kearney */
/* Homework 1, Problem 6 */

#include <stdio.h>

// function prototype
void calcsum( int n , int A[] );

int main(  ) {
	
	int A[ 8 ] = { 17, -3, 8, 49, -5, 56, 100, -22 };
	
	calcsum( 4, A );
	calcsum( 6, A );

	return 0;
}

// functions
void calcsum( int n , int A[] ) {

	printf( "The sum of the first %d values is ", n );

	int sum = 0;
	while( n > 0 ) {
		sum += A[ n-1 ];
		n--;
	}

	printf( "%d.\n", sum );

}