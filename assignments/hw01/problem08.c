/* Author: Reilly Kearney */
/* Homework 1, Problem 8 */

#include <stdio.h>

// function prototype
unsigned fib( unsigned n );

int main(  ) {
	
	unsigned term, n = 5;
	term = fib( n );

	return 0;
}

// functions
unsigned fib( unsigned n ) {
	printf( "The memory address of n is %p.\n", &n );
	if ( n == 0 ) 
		return 0;
	else if ( n == 1 )
		return 1;
	else
		return ( fib( n - 1 ) + fib( n - 2 ) );
}