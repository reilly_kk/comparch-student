/* Author: Reilly Kearney */
/* Homework 2, Problem 1 */

#include <stdio.h>
#include <stdlib.h>

int A;
int B = 0;
int array1[1024];

void func1() {
	int func1_0, func1_1, func1_2;
	// Question 7
	printf( "The memory address of func1_0: %p\n", &func1_0 );
	printf( "The memory address of func1_1: %p\n", &func1_1 );
	printf( "The memory address of func1_2: %p\n", &func1_2 );
}

void func2( int func2_0, int func2_1, int func2_2 ) {
	// Question 8
	printf( "The memory address of func2_0: %p\n", &func2_0 );
	printf( "The memory address of func2_1: %p\n", &func2_1 );
	printf( "The memory address of func2_2: %p\n", &func2_2 );
}

int main() {
	char C;
	int D = 0;
	char array2[1024];
	long array3[1024];
	short array4[1024];
	func1();
	
	int E, F, G;
	func2(E, F, G);
	
	char *allocated = (char *)malloc(1024);
	
	// Question 1
	printf( "The memory address of A : %p\n", &A );
	printf( "The memory address of B : %p\n", &B );
	printf( "The memory address of array1 : %p\n", &array1 );
	
	// Question 2
	printf( "Size of array1 : %d \n", sizeof( array1 ) );

	// Question 3
	printf( "The memory address of D : %p\n", &D );
	printf( "The memory address of C : %p\n", &C );
	printf( "The memory address of array2 : %p\n", &array2 );
	
	// Question 4
	printf( "Size of array2 : %d\n", sizeof( array2 ) );
	
	// Question 5
	printf( "Size of array3 : %d\n", sizeof( array3 ) );
	
	// Question 6
	printf( "Size of array4 : %d\n", sizeof( array4 ) );

	// Question 10


	// Question 11
	printf( "The memory address of func1 : %p\n", &func1 );
	printf( "The memory address of func2 : %p\n", &func2 );
}