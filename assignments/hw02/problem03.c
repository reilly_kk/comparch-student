/* Author: Reilly Kearney */
/* Homework 2, Problem 3 */

#include <stdio.h>

int logicfcn( int a );

int main () {

	int output;
	output = logicfcn( 43981 );

	printf( "unsigned: %u\thex: %x\n", output, output );

	return 0;
}

int logicfcn( int a ) {
	int v = a >> 4;
	v = v & 255;
	return v;
}