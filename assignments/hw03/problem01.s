# problem1.s

	.set noreorder
	.data
	.text
	.globl main
	.ent main

main:
	addi $s0, $0, 84 			# int score = 84;
	addi $s1, $0, 0 			# int grade;

								# if
	slti $t0, $s0, 90			#	(score >= 90)
	bne $t0, $0, elseif_1		# if score < 90, elseif_1
	nop
	addi $s1, $0, 4 			#	grade = 4;
	j print 					# jump to print

elseif_1:						# else if
	slti $t1, $s0, 80			#	(score >= 80)
	bne $t1, $0, elseif_2		# if score < 90, elseif_2
	nop
	addi $s1, $0, 3				#	grade = 3;
	j print						# jump to print
	
elseif_2:						# else if	
	slti $t2, $s0, 70			#	(score >= 70)
	bne $t2, $0, else 			# if score < 90, else
	nop
	addi $s1, $0, 2 			# 	grade = 2;
	j print 					# jump to print
	
else:	# else	
	addi $s1, $0, 0				#	grade = 0;
	j print 					# jump to print
	
print:	# PRINT_HEX_DEC(grade);
	add $a0, $0, $s1
	ori $v0, $0, 20 			# print syscall
	syscall
	nop

exit:	# EXIT;
		ori $v0, $0, 10			# exit
		syscall
	.end main
