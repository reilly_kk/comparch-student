#problem2.s

	.set noreorder
	.data
	A: .byte 1, 25, 7, 9, -1 	#char A[] = { 1, 25, 7, 9, -1 };
	.text
	.globl main
	.ent main

main:

	la $s0, A 					# load A into s0
	addi $s1, $0, 0				# int i = 0;
	lb $s2, 0($s0)				# current = A[0];
	addi $s3, $0, 0				# int max = 0;
		
loop_cond:						# while
	slt $t0, $0, $s2			#	(current > 0)
	beq $t0, $0, print 			# go to print if <= 0
	nop
	slt $t1, $s3, $s2			#	if (current > max)
	beq $t1, $0, back_loop 		# go to print if <= 0
	nop
	add $s3, $0, $s2			#	max = current;
	
back_loop:
	addi $s1, $s1, 1 			#	i = i + 1;
	addi $s0, $s0, 1 			# increment to access next A[i] element
	lb $s2, 0($s0)				# load A[i] into current
	j loop_cond

print:							# PRINT_HEX_DEC(max);
	add $a0, $0, $s3
	ori $v0, $0, 20				# print syscall
	syscall

exit:							# EXIT;
		ori $v0, $0, 10
		syscall
	.end main
