# problem3.s

	.set noreorder
	.data
	A:.space 32			# int A[8], 32 for bytes (8x4bytes)
	.text
	.globl main
	.ent main

main:

	la $s0, A           # load array into register
	addi $s1, $0, 0     # A[0] = 0
	sw $s1, 0($s0)      # set register $s1 to memory holding A[0]
	addi $s2, $0, 1     # A[1] = 1
	sw $s2, 4($s0)      # set register $s1 to memory holding A[1]
	addi $s0, $s0, 4    # in memory, go to next int
	addi $s3, $0, 2     # (i = 2)

for_loop:				# for
	slti $t1, $s3, 8    # 	(i < 8)
	beq $t1, $0, exit   # 	if (i >= 8), exit loop
	nop

	add $s4, $s2, $s1   # A[i] = A[i-1] + A[i-2];
	addi $s0, $s0, 4    # move 4 bytes to next value in array
	sw $s4, 0($s0)      # load value into reg

	add $s1, $0, $s2    # increment A[0]
	add $s2, $s0, $s4   # increment A[1]
	addi $s3, $s3, 1    # increment i

	# print
	add $a0, $0, $s4
	ori $v0, $0, 20     # print syscall
	syscall
	nop
	j for_loop
	nop

exit:					# exit
		ori $v0, $0, 10
		syscall
	.end main
