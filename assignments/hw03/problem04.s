# problem4.s

	.set noreorder
	.data
	.text
	.globl main
	.ent main

record:							# typedef struct record
	record.field0: .space 4		#     int field0;
	record.field1: .space 4		#     int field1;

main:
	addi $a0, $0, 8				# size of record is 8, $a0 = 8
	ori $v0, $0, 9				# malloc syscall - 9
	syscall
	addi $s0, $v0, 0			# stores $v0 address

	addi $t0, $0, 0				# set record location to first element
	addi $t1, $0, 100			# $t1 = 100
	sw $t1, record($t0)			# store $t1 in record->field0

	addi $t0, $t0, 4			# move record location to second element
	addi $t2, $0, -1 			# $t2 = -1
	sw $t2, record ($t0)		# store $t2 in record->field1
	
exit:							# EXIT;
		ori $v0, $0, 10
		syscall
	.end main
