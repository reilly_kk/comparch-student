# problem5.s
	.set noreorder
	.data
	.text
	.globl main
	.ent main
main:
						# newelt = (elt *) MALLOC(sizeof (elt));
	addi $a0, $0, 8			# sizeof(elt) = 8 bytes
	ori $v0, $0, 9			# malloc syscall - 9
	syscall
						# newelt->value = 1;
	addi $t0, $0, 1
	sw $t0, 0($v0)			# set $t0 to memory($v0 + 0)					
						# newelt->next = 0;
	addi $v0, $v0, 4
	addi $t1, $0, 0
	sw $t1, 0($v0)			# set $t1 to memory($v0 + 0)
						# head = newelt;
	addi $v0, $v0, -4 		
	add $v1, $0, $v0 		
						# newelt = (elt *) MALLOC(sizeof (elt));
	addi $a0, $0, 8			# set size of elt to 8 bytes
	ori $v0, $0, 9			# malloc syscall - 9
	syscall
						# newelt->value = 2;
	addi $t2, $0, 2 		
	sw $t2, 0($v0)			# set $t2 to memory($v0 + 0)
						# newelt->next = head;
	addi $v0, $v0, 4 
	sw $v1, 0($v0)			# set $v1 to memory($v0 + 1)
						# head = newelt;
	addi $v0, $v0, -4	
	add $a1, $8, $v0
						# PRINT_HEX_DEC(head->value);
	lw $t3, 0($a1)				
	add $a0, $t3, $8			
	ori $v0, $0, 20			# print syscall
	syscall
						# PRINT_HEX_DEC(head->next->value);
	addi $a1, $a1, 4
	lw $t4, 0($a1)				
	add $a0, $t4, $0			
	ori $v0, $0, 20			# print syscall
	syscall
exit:					# EXIT;
		ori $v0, $0, 10     
		syscall
	.end main
