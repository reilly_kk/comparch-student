# problem7.s

	.set noreorder
	.data
	.text
	.globl main
	.ent main

print:	# void print(int a)
		# v0 = a
	ori $v0, $0, 20		# print syscall
	syscall
	jr $ra
	nop

sum3:   # int sum3(int a, int b, int c)
		# a0 = a, a1 = b, a2 = c
	add $t7, $a0, $a1   # s0 = a0 + a1
	add $t7, $t7, $a2   # s0 = s0 + a2
	add $a3, $0, $t7    # a0 = s0
	ori $v0, $0, 20		# return a+b+c;
	jr $ra
	nop

polynomial: # int polynomial(int a, int b, int c, int d, int e)
			# int x = t0, y = t1, z  = t2
						# a0 = a, a1 = b
	sllv $t0, $a0, $a1 	# x = a << b;
						# a2 = c, a3 = d
	sllv $t1, $a2, $a3 	# y = c << d;
						# z = sum3(x, y, e);
	addi $a0, $t0, 0 	# store x(t0) in a0
	addi $a1, $t1, 0 	# store y(t1) in a1
	addi $a2, $t4, 0 	# store e(t4) in a2
	jal sum3
	nop
	addi $t2, $a3, 0    # store value of sum in temp reg
						# print(x);
	addi $a0, $t0, 0 
	jal print
	nop
						# print(y);
	addi $a0, $t1, 0 
	jal print
	nop
						# print(z);
	addi $a0, $t2, 0
	jal print
	nop
						# return z;
	addi $a0, $a3, 0 
	jr $t9              # returns z
	nop

main:

	addi $s0, $0, 2     # int a = 2;
						# int f = polynomial(a, 3, 4, 5, 6);
	add $a0, $0, $s0    # a0 = s0
	addi $a1, $0, 3     # a1 = 3
	addi $a2, $0, 4     # a2 = 4
	addi $a3, $0, 5     # a3 = 5
	addi $t4, $0, 6     # t4 = 6
	jal polynomial      # call function
	addi $t9, $ra, 0    
	addi $t5, $a0, 0
						# print(a);
	addi $a0, $s0, 0
	jal print
	nop
						# print(f);
	addi $a0, $t2, 0
	jal print
	nop

exit: 	# exit
		ori $v0, $0, 10    
		syscall
	.end main
